
import java.util.Scanner;

public class OxGame {

    static Scanner kb = new Scanner(System.in);
    static String[][] table = new String[3][4];
    static String[] x = {"1", "2", "3"};

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
        System.out.println("");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = "-";
            }
        }
    }

    static void showTable() {
        for (int i = 0; i < 3; i++) {
            System.out.print(" " + x[i]);
        }
        System.out.println();
        for (int i = 0; i < 3; i++) {
            System.out.print(x[i]);
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }
    static String turn = "X";

    static String showTurn() {
        return turn;
    }

    static void inputInva() {
        System.out.println("Not found, pls try again, select other position");
        System.out.println("Please input Row Col : ");
    }

    static void input(int a, int b) {
        while ((a < 1 || a > 3 || b < 1 || b > 3)) {
            inputInva();
            a = kb.nextInt();
            b = kb.nextInt();
        }
        if (table[a - 1][b - 1].equals("-")) {
            table[a - 1][b - 1] = turn;
        } else {
            while (!table[a - 1][b - 1].equals("-")) {
                inputInva();
                a = kb.nextInt();
                b = kb.nextInt();
                while ((a < 1 || a > 3 || b < 1 || b > 3)) {
                    inputInva();
                    a = kb.nextInt();
                    b = kb.nextInt();
                }
            }
            table[a - 1][b - 1] = turn;
        }
    }
    static int xcount_x = 0, ycount_x = 0, ldiag_x = 0, rdiag_x = 0;
    static int xcount_o = 0, ycount_o = 0, ldiag_o = 0, rdiag_o = 0;
    static boolean check = true;
    static String checkwin;

    static String checkWin() {
        if (checkRowcol().equals("X") || checkX().equals("X")) {
            showTable();
            checkwin = "Player X win .....";
            check = false;
        }
        else if (checkRowcol().equals("O") || checkX().equals("O")) {
            showTable();
            checkwin = "Player O win .....";
            check = false;
        } else {
            checkwin = "";
        }
        return checkwin;
    }

    static void checkDraw(int n) {
        if (n == 9 && check == true) {
            showTable();
            System.out.println("Draw.....");
        }
    }
    static String va=  " ";

    static String checkRowcol() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j].equals("X")) {
                    xcount_x++;
                }
                if (table[i][j].equals("O")) {
                    xcount_o++;
                }
                if (table[j][i].equals("X")) {
                    ycount_x++;
                }
                if (table[j][i].equals("O")) {
                    ycount_o++;
                }
            }
            if (xcount_x == 3 || ycount_x == 3) {
                va=  "X";
                break;
            }else if (xcount_o == 3 || ycount_o == 3) {
                
                va=  "O";
                break;
            } else {
                xcount_x = 0;
                xcount_o = 0;
                ycount_x = 0;
                ycount_o = 0;
            }
        }
        return va;
    }

    static String checkX() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0, jj = 2; j < 3; j++, jj--) {
                if (table[j][j].equals("X")) {
                    ldiag_x++;
                }
                if (table[j][jj].equals("X")) {
                    rdiag_x++;
                }
                if (table[j][j].equals("O")) {
                    ldiag_o++;
                }
                if (table[j][jj].equals("O")) {
                    rdiag_o++;
                }
            }
            if (ldiag_x == 3 || rdiag_x == 3) {
                va=  "X";
                break;
            } else if (ldiag_o == 3 || rdiag_o == 3) {
                va=  "O";
                break;
            } else {
                ldiag_x = 0;
                rdiag_x = 0;
                ldiag_o = 0;
                rdiag_o = 0;
            }
        }
        return va;
    }

    static String switchPlayer() {
        if (turn.equals("X")) {
            turn = "O";
        } else if (turn.equals("O")) {
            turn = "X";
        }
        return turn;
    }

    public static void main(String[] args) {
        showWelcome();
        int input1, input2;
        int i = 0;
        while (check == true && i != 9) {
            showTable();
            System.out.println("");
            System.out.println(showTurn() + " turn");
            System.out.println("Please input Row Col : ");
            input1 = kb.nextInt();
            input2 = kb.nextInt();
            input(input1, input2);
            System.out.println(checkWin());
            i++;
            switchPlayer();
        }
        checkDraw(i);
        System.out.println("Bye bye . . . .");

    }

}
